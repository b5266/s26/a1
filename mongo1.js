db.fruits.aggregate([
	{ $match: {onSale: true}},
    { $group: { _id: "$onSale", avgStockOnSale: { $avg: "$stock" } } }
]);

db.fruits.aggregate([
	 { $group: { _id: "$suppier_id", avgStock: { $avg: "$stock" } } } 
]);
