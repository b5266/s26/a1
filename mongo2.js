db.fruits.aggregate([
	{ $match: { onSale: true} },
	{$group:{_id: "$onSale",maxPriceFruitOnSale: { $max: "$price"} } }
]);

db.fruits.aggregate([
	{ $match: {origin: "Philippines"}},
    {$group:{_id: "$Philippines",maxPriceInPhilippines: { $max: "$price"} } }
]);
